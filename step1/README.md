# Containerisation d'une application SpringBoot
## 1. Contexte
Afin de facilité le déploiement et le cycle de vie de notre application, nous allons créer un container docker associé à notre application (ici Springboot).
Cette étape est inspiré du tuto de [javainuse](https://www.javainuse.com/devOps/docker/docker-jar)

Le workflow suivant sera réalisé:

<img src="./img/DockerComposeStep1-Archi.jpg" alt="Step 1 Workflow" width="600"/>

## 2. DockerFile
- Récupérer le contenu de l'application disponible ici [step0](../step0/pom.xml)
- au même niveau que votre fichier ```pom.xml``` créer un fichier nommé ```Dockerfile```. Ce fichier va contenir les instructions permettant de construire notre container docker. Créé le fichier comme suit:

```
FROM openjdk:8
RUN mkdir -p /app/SP
WORKDIR /app/SP
COPY ./target/SP-Compose-Step1-0.0.1-SNAPSHOT.jar SP-Compose-Step1-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","SP-Compose-Step1-0.0.1-SNAPSHOT.jar"]
```
- Explications:
    - ```FROM```: spécifie à partir de quelle image nous allons créer notre propre image (ici nous allons composer à partir de l'image docker ```openjdk:8```)
    - ```RUN```: permet d'exécuter une commande sur notre nouveau container. Ici nous allons créer le répertoire ```/app/SP```.
    - ```WORKDIR```: permet de nous déplacer dans le répertoire spécifié. Ici nous allons nous déplacer dans ```/app/SP```.
    - ```CMD```: cette commande permet de démarrer une application de notre container avec les arguments adéquats. ```CMD``` prend la forme suivante: ["executable", "param1", "param2"…]. Dans notre cas, nous allons lancer la commande ```java``` pour exécuter notre application passée en paramètres.
    - Une description complète des docker file est disponible ici [dockerfile](https://docs.docker.com/engine/reference/builder/)


## 2. Préparation de l'application Springboot
- Afin de permettre le déploiement de l'application effecter la modification suivante dans le fichier ```application.properties```

```yaml
...
#server.address=127.0.0.1
...
```
- Explication
  - en commentant ```server.address``` nous laissons la configuration par défaut de springboot permettant de bind l'ensemble des adresses ```0.0.0.0```

- Compiler et générer le .jar de l'application

```
mvn clean install
```

## 3. Création de notre image Docker
- Une fois notre ```Dockerfile``` créé, nous allons demander à docker de créer une image docker à partir de ce fichier
- Dans un terminal, aller dans le dossier contenant votre ```Dockerfile```, puis taper la commande suivante:

```
docker image build -t hero-app:v1 .
```
- Explications
  - ```docker image build```: commande de base permettant de créer une image docker à partir d'un dockerfile (liste complète des options disponibles [ici](https://docs.docker.com/engine/reference/commandline/build/))
  - ```-t hero-app:v1```: permet de nommer notre image. Les images de docker sont toujours de la forme ```<name>:<tag>```.
  - ``` . ```: indique à docker qu'un fichier ```Dockerfile``` est disponible dans le répertoire courant. L'option ```-f``` permet de spécifier un autre fichier (autre nom et emplacement).

- Démarrer le container créé:

```
docker container run --name runner-hero -p 8081:8081 -d hero-app:v1
```
- Explication
   - ```run``` permet de démarrer un container à partir d'une image ici ```hero-app:v1```
   - ```--name``` permet de nommer la réunion 
   -  ``` -p -p 8081:8081 ``` : permet de lier les ports du container docker à la machine hôte
   -  ```-d``` permet de détacher le container et le démarrer en background.

- Vérifier que le container est bien démarré. 

```
docker ps
   CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
   2a11087f4022        hero-app:v1         "java -jar SP-Compos…"   39 minutes ago      Up 39 minutes       0.0.0.0:8081->8081/tcp   runner-hero
```

```
docker container logs runner-hero

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.7.RELEASE)

2020-07-21 13:07:13.869  INFO 1 --- [           main] com.compose.app.HeroApp                  : Starting HeroApp v0.0.1-SNAPSHOT on 2a11087f4022 with PID 1 (/app/SP/SP-Compose-Step1-0.0.1-SNAPSHOT.jar started by root in /app/SP)
...
2020-07-21 13:07:30.799  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8081 (http) with context path ''

```
- Sur la machine host effectuer la requête suivante sur un WebBrowser

<img src="./img/dockerHeroRequest.jpg" alt="dockerHeroRequest" width="400"/>



