# Automatisation d'un déploiement d'application à l'aide de Docker Compose

## 1. Contexte
Dans cette section nous allons mettre en oeuvre 2 containers, 1 container contenant notre application ```HeroApp``` et un container contenant la base de données ciblée par ```HeroApp```.
Ce tutoriel se base sur le tutoriel [javainuse](https://www.javainuse.com/devOps/docker/docker-compose-tutorial).
L'architecture suivante sera mise en oeuvre :

<img src="./img/DockerComposeStep2-Archi.jpg" alt="Architecture Docker Step2" width="600"/>

## 2. Mise en place de docker compose
- Docker compose permet de définir et de démarrer plusieurs containers Docker pouvant interagir entre eux. Comme évoque sur la documentation [officielle](https://docs.docker.com/compose/), utilisé docker compose peut se résumer en 3 étapes:
  - Définir un container permettant d'exécuter notre application (Dockerfile fait en [step1](../step1/README.md))
  - Définir des services permettant de démarrer nos container d'application (à l'aide d'un fichier de configuration yaml ```docker-compose.yml```)
  - Démarrer docker compose à l'aide de notre fichier de configuration permettant le démarrage de l'écosystème de nos applications

### 2.1 Incorporer notre application vis docker compose
- Créer le fichier ```docker-compose.yml``` à la racine de votre projet (le fichier peut être créé n'importe où) comme suit:

```yaml
version: "3"
services:
  heroapp:
    image: hero-app:v1
    ports:
      - "8081:8081"
    networks:
      - hero-network

networks:
  hero-network:
```
- Cette configuration va simplement démarrer notre image vis docker composer
- A l'emplacement de votre fichier docker-compose.yml lancer votre docker compose par la commande suivante:

```
docker-compose up
```
- Votre container démarre, vérifier que votre application est toujours accessible.


<img src="../step1/img/dockerHeroRequest.jpg" alt="dockerHeroRequest" width="400"/>

### 2.2 Dissocier la base de données de notre application
- Dans cette section nous allons définir un container indépendant portant la base de données.
- Modifier le fichier ```pom.xml``` de notre application comme suit:

```xml
...
<!-- 		<dependency> -->
<!-- 			<groupId>com.h2database</groupId> -->
<!-- 			<artifactId>h2</artifactId> -->
<!-- 			<scope>runtime</scope> -->
<!-- 		</dependency> -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<scope>runtime</scope>
		</dependency>
    ...
```
- Explications:
  - Nous allons utiliser une base de données Msql avec notre application et non plus la base de données embarquée H2

- modifier le fichier ```application.properties``` dans le package /```src/main/resources``` de votre application comme suit:

```yaml
# Bind all
#server.address=127.0.0.1
server.port=8081
spring.jpa.hibernate.ddl-auto=update
```
- Explications:
  - ```spring.jpa.hibernate.ddl-auto=update```: va permettre de mettre à jour la base de données si nécessaire

- Supprimer le fichier ```data.sql``` dans ```src/main/resources```. La base n'étant pas locale, Springboot va tenter de mettre à jour la BD avant que cette dernière soit prête.

- Modifier le fichier ```docker-compose``` comme suit:
```yaml
version: "3"
services:
  heroapp:
    image: hero-app:v1
    ports:
      - "8081:8081"
    depends_on: 
      - db # This service depends on mysql. Start that first.
    environment: # Pass environment variables to the service
      SPRING_DATASOURCE_URL: jdbc:mysql://db:3306/herobd?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false
      SPRING_DATASOURCE_USERNAME: heroname
      SPRING_DATASOURCE_PASSWORD: heropwd
    networks:
      - hero-network
  db:
    image: mysql:5.7
    ports:
      - "3306:3306"
    restart: always
    environment:
      MYSQL_DATABASE: herobd
      MYSQL_USER: heroname
      MYSQL_PASSWORD: heropwd
      MYSQL_ROOT_PASSWORD: heroRootPwd

    networks:
      - hero-network

networks:
  hero-network:
```

- Ajouter un hero dans l'application avec la commande suivante:

```
curl --header "Content-Type: application/json" --request POST --data '{ "name":"Thor","superPowerName":"Storm","superPowerValue":100,"imgUrl":"https://i.gifer.com/D2Tc.gif"}' http://localhost:8081/hero
```

- Vérifier que le Hero a bien été ajouté:

<img src="./img/dockerComposeHeroRequest.jpg" alt="dockerHeroRequest" width="400"/>

- A cette étape notre ```HeroApp``` peut accéder au container ```Db``` et à l'extérieur. Dans cette configuration le container ```Db``` peut également être accédé à l'extérieur, ce qui n'est pas souhaitable pour des raisons de sécurité.
- Modifier le ```docker-compose.yml``` afin d'isoler la communication ```HeroApp```<->```Db```

```yaml
version: "3"
services:
  heroapp:
    image: hero-app:v1
    ports:
      - "8081:8081"
    depends_on: 
      - db # This service depends on mysql. Start that first.
    environment: # Pass environment variables to the service
      SPRING_DATASOURCE_URL: jdbc:mysql://db:3306/herobd?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false
      SPRING_DATASOURCE_USERNAME: heroname
      SPRING_DATASOURCE_PASSWORD: heropwd
    networks:
      - hero-network-private
      - hero-network-public
  db:
    image: mysql:5.7
    ports:
      - "3306:3306"
    restart: always
    environment:
      MYSQL_DATABASE: herobd
      MYSQL_USER: heroname
      MYSQL_PASSWORD: heropwd
      MYSQL_ROOT_PASSWORD: heroRootPwd

    networks:
      - hero-network-private

networks:
  hero-network-private:
    internal: true
  hero-network-public:
```
- Explications:
    ```yaml
    ...
    services:
    heroapp:
        image: hero-app:v1
        ...
        networks:
        - hero-network-private
        - hero-network-public
    ```
    - Permet de définir plusieurs niveaux de réseaux disponibles pour le container ```heroapp```
    ```yaml
    ...
    services:
    ...
    db:
        ...
        networks:
          - hero-network-private
    ```
    - Permet de définir un réseau spécific au container ```db``` qui ne sera pas exposé à la machine hôte

    ```yaml
    ...
    networks:
       hero-network-private:
           internal: true
       hero-network-public:
    ```
    - Deux réseaux sont définis:
      - ```hero-network-private``` accessible uniquement entre les containers grâce à l'option ```internal: true```
      - ```hero-network-public``` sera accessible par les containers et la machine hôte.

- Redéployer docker compose
```
docker-compose up
```
- Vérifier que le container Db n'est plus accessible

```
telnet localhost 3306
Trying 127.0.0.1...
telnet: Unable to connect to remote host: Connection refused
```