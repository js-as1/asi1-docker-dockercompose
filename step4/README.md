#  Création automatique d'images docker à l'aide de CI/CD de Gitlab
## 1. Contexte
Dans cette section nous allons mettre en place une intégration continue permettant de créer et stocker automatiquement les images dockers sur l'espace de stockage prévu à cet effet dans Gitlab (Gitlab `Container Registry`). 
Des informations plus détaillées sur la mise en place de l'intégration continue notamment avec SpringBoot sont disponibiles sur la step 3 du tutoriel `asi1-springboot-tuto` [step3](https://gitlab.com/js-as1/asi1-springboot-tuto/-/blob/master/step3/README.md).
Dans la suite de ce tutoriel, nous partirons du princpe que votre projet est versioné dans un repository Gitlab.
<!---
## 2. Création des Credential 
- Git lab offre possibilité de stocker des images docker sur un `container registery`.  L'interaction avec le `container registery` peut se faire via ligne de commande ou en utilisant l'Intégration Continue et le Déploiement Continue de Gitlab. Dans les deux cas, il faut générer des credentials pour pouvoir y accéder

### 2.1 Créer un Deploy Token 
- Aller dans la section `Settings > Repository`
- Etendre la section `Deploy Token`
- Choisir un nom, une date d'expiration et éventuellement un username
- Dans la section `Scopes`, sélectionner `read_registry` et `write_registry` 
- Valider en cliquant sur le bouton `Create deploy token `

<img src="./img/DockerGitLabDeployKeyv1.png" alt="User List" width="600"/>

- Conserver les informations qui vous sont fourniées

<img src="./img/DockerGitLabDeployKeyv2.png" alt="User List" width="600"/>


### 2.2 Ajout de variables au repository
- Aller dans section `Settings > CI / CD Settings`
- Etendre la section `Variables`
- Cliquer sur le bouton `Add Variable`

- Ajouter une nouvelle clé de nom `CI_REGISTRY_USER` et de valeur le nom d'utilisateur de votre `Deploy Token` (gitlab+deploy-token-5), sélectionner l'option `Mask variable` et valider la création de votre variable

- Ajouter une nouvelle clé de nom `CI_REGISTRY_USER_PASSWORD` et de valeur le password du  `Deploy Token`, sélectionner l'option `Mask variable` et valider la création de votre variable


<img src="./img/DockerGitLabVariableEditionv1.png" alt="Variable Edition" width="400"/>


<img src="./img/DockerGitLabVariablesListv2.jpg" alt="Variable List" width="600"/>

- Ces variables créées nous permettront d'interagir avec le `Container Registery` de gilatb
-->

## 2. Mise en CI / CD de gitlab
### 2.1 Création du fichier `.gitlab-ci.yml`
- Le fichier `.gitlab-ci.yml` contient l'ensemble des informations nécessaires à GitLab pour effectuer l'intégration continue et le déploiement continue.
- Une description détaillée de la configuration de ce fichier est disponible sur la [documentation officielle Gitlab](https://docs.gitlab.com/ee/ci/yaml/).

- Dans ce fichier nous allons utiliser les variables d'environnement disponibles dans Gitlab: `CI_REGISTRY_USER` et `CI_REGISTRY_PASSWORD` qui vont permettre d'avoir un accès en lecture/écriture sur le ̀`Container Registry` de Gitlab et qui seront valides uniquement pour le job concerné (cf. https://gitlab.com/help/user/packages/container_registry/index).


- Créer le fichier `.gitlab-ci.yml` à la racine de votre repository comme suit:

```yaml 
stages:
  - package
  - build

job_package:
  image: maven:3-jdk-8
  stage: package
  script:
    - mvn -f micro_service_hero/pom.xml package
    - mvn -f micro_service_user/pom.xml package
    - mvn -f proxy_server/pom.xml package
  artifacts:
    paths:
    - micro_service_hero/target/*.jar
    - micro_service_user/target/*.jar
    - proxy_server/target/*.jar
    expire_in: 1 week

job_build:
  image: docker:19.03.12
  stage: build
  services:
    - docker:19.03.12-dind
  script:
    - mkdir target
    - cp micro_service_hero/target/*.jar target/.
    - cp micro_service_user/target/*.jar target/.
    - cp proxy_server/target/*.jar target/.
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/micro_service_hero:latest -f ./micro_service_hero/Dockerfile .
    - docker build -t $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/micro_service_user:latest -f ./micro_service_user/Dockerfile .
    - docker build -t $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/proxy_server:latest -f ./proxy_server/Dockerfile .
    - docker push $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/micro_service_hero:latest
    - docker push $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/micro_service_user:latest
    - docker push $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/proxy_server:latest
  only:
    - master

```

- Explications:

  ```yaml
  stages:
  - package
  - build
  ...
  ```
  - les `stages` représentent les différentes étapes de l'intégration continue. Dans notre cas simplifié deux étapes seront effectuées successivement:
    - `package`: qui va permettre la compilation de nos projets SpringBoot
    - `build`: qui va permettre de créer nos images docker associées aux services Springboot et les enregistrer dans le `container repository` de Gitlab.
  ```yaml
  ...
    job_package:
      image: maven:3-jdk-8
      stage: package
      script:
        - mvn -f micro_service_hero/pom.xml package
        - mvn -f micro_service_user/pom.xml package
        - mvn -f proxy_server/pom.xml package
      artifacts:
        paths:
        - micro_service_hero/target/*.jar
        - micro_service_user/target/*.jar
        - proxy_server/target/*.jar
        expire_in: 1 week
    ...
    ```
    - `job_package`: définit un bloc de commandes a effectuer par l'intégration continue
    - `image: maven:3-jdk-8`: dans ce bloc, les opérations seront effectuées dans un container docker dont l'image est `maven:3-jdk-8`. Cette image contient entre autre un jdk8 et maven comme outils nous permettant de compiler nos projets.

   - `stage: package`: indique à la CI de Gitlab que ce bloc de commandes, ce job, sera a effectuer durant l'étape `package`.

   - `script`: contient l'ensemble des commandes à effecuter sur le container `maven:3-jdk-8`. Nous allons ainsi compiler l'ensemble de nos projets SpringBoot et générer les `jar` associés à l'aide de la commande maven `mvn package`.
   - `artifacts:` permet de préciser les fichiers modifiés qui seront conservés durant l'intégration continue. Ces fichiers seront notamment transmists aux autres étapes de la CI. Ici nous conservons l'ensemble des jar générés par la compilation de nos projets SpringBoot.

  ```yaml
  ...
    job_build:
      image: docker:19.03.12
      stage: build
      services:
        - docker:19.03.12-dind
      script:
        - mkdir target
        - cp micro_service_hero/target/*.jar target/.
        - cp micro_service_user/target/*.jar target/.
        - cp proxy_server/target/*.jar target/.
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build -t $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/   micro_service_hero:latest -f ./micro_service_hero/Dockerfile .
        - docker build -t $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/   micro_service_user:latest -f ./micro_service_user/Dockerfile .
        - docker build -t $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/   proxy_server:latest -f ./proxy_server/Dockerfile .
        - docker push $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/   micro_service_hero:latest
        - docker push $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/   micro_service_user:latest
        - docker push $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/   proxy_server:latest
      only:
        - master

  ```
  - `job_build`: définit un bloc de commandes, job,  a effectuer par l'intégration continue
  - `image: docker:19.03.12`: dans ce bloc, les opérations seront effectuées dans un container docker dont l'image est `docker:19.03.12`. Cette image contient entre autre les outils `docker in docker` permettant de construire une image docker à l'aide d'un dockerfile

  - `stage: build`: indique à la CI de Gitlab que ce bloc de commandes sera a effectuer durant l'étape `build`. L'ensemble des `artifacts` (l'ensemble des .jar) définis dans l'étape précédente, `package`, seront accessibles.
  - `services: - docker:19.03.12-dind`. Les services sont des images dockers qui seront démarrés pendant l'exécution du job courant. Ces images vont contenir des services/outils utilisables durant l'étape courante.

  - `script`: contient l'ensemble des commandes à effecuter sur le container 
      ```yaml
      ...
        - mkdir target
        - cp micro_service_hero/target/*.jar target/.
        - cp micro_service_user/target/*.jar target/.
        - cp proxy_server/target/*.jar target/.
        ...
      ```
      - Un répertoire `target` est créé et l'on va copier l'ensemble des `.jar` dans ce répertoire
      ```yaml
      ...
         - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
      ...
      ```
      - Connexion au `container registery` de gitlab à l'aide des `credentials` disponibles par defaut.
      ```yaml
      ...
        - docker build -t $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/   micro_service_hero:latest -f ./micro_service_hero/Dockerfile .
      ...
      ```
      - Création des images dockers à l'aide des différents `Dockerfile`. Cette opréation est effectuée pour l'ensemble de nos services SpringBoot

      ```yaml
      ...
        -  docker push $CI_REGISTRY/js-as1/asi1_docker_dockercompose_ci_example/   micro_service_hero:latest
      ...
      ```
      - Envoie les images docker compilées au `container registery` de gitlab.
  - `only: - master`: spécifie à Gitlab CI que ce job ne sera effectué que sur la branch master

### 2.2 Tester la création des images dockers
- Une fois le fichier `.gitlab-ci.yml` créé, valider et envoyer vos changements (`commit` et `push`)
- Dans l'interface Web de Gitlab aller dans `CI / CD > PipeLines`

<img src="./img/DockerGitLabPipeLinev1.png" alt="Docker GitLab Pipe Line" width="600"/>

- Vérifier que les images ont bien été stockées dans `Packages & Registries > Container Registery`

<img src="./img/DockerGitLabContainerRegisteryv1.png" alt="Docker GitLab Container Registery" width="600"/>

- Dans un terminal vérifier que vous arrivez à récupérer l'image docker:

```
$ docker image pull registry.gitlab.com/js-as1/asi1_docker_dockercompose_ci_example/micro_service_user

Using default tag: latest
latest: Pulling from js-as1/asi1_docker_dockercompose_ci_example/micro_service_user
57df1a1f1ad8: Pull complete 
71e126169501: Pull complete 
1af28a55c3f3: Pull complete 
03f1c9932170: Pull complete 
881ad7aafb13: Pull complete 
db6a1d457bc3: Pull complete 
b7fe4ef08350: Pull complete 
084ac4d506de: Pull complete 
75282165adf8: Pull complete 
Digest: sha256:6088f2944963c8d7ca748b5a69893a807eb8a56b2f89337870839bfb9756f236
Status: Downloaded newer image for registry.gitlab.com/js-as1/asi1_docker_dockercompose_ci_example/micro_service_user:latest
registry.gitlab.com/js-as1/asi1_docker_dockercompose_ci_example/micro_service_user:latest

```

## 3. Modification du docker-compose
- Nous avons maintenant à notre disposition les images docker à jour de nos services SpringBoot stockées sur le Container Registry de Gitlab. Nous allons modifier le fichier `docker-compose.yml` afin qu'il soit autonome et ne dépende plus du repository.
- Appliquer les modifications suivantes au fichiers `docker-compose.yml`


```yaml
  ...
  proxyapp:
    #build:
    #    context: proxy_server # utilise un dockerfile and un répertoire particulier
    #    dockerfile: Dockerfile
    image: registry.gitlab.com/js-as1/asi1_docker_dockercompose_ci_example/proxy_server
    ports:
      - "8000:8000"

    ...

  heroapp:
    #build:
    #  context: micro_service_hero # utilise un dockerfile and un répertoire particulier
    #  dockerfile: Dockerfile
    image: registry.gitlab.com/js-as1/asi1_docker_dockercompose_ci_example/micro_service_hero
    ports:
      - "8081:8081"

    ...

    userapp:
    #build:
    #  context: micro_service_user # utilise un dockerfile and un répertoire particulier
    #  dockerfile: Dockerfile
    image: registry.gitlab.com/js-as1/asi1_docker_dockercompose_ci_example/micro_service_user
    ports:
      - "8082:8082"

    ...

```

- Démarrer l'ensemble des containers en utilisant docker-compose
```
docker-compose up
```

- Ajouter un utilisateur via la commande curl suivante:
```
curl --header "Content-Type: application/json" --request POST --data '{ "surName":"John","lastName":"Doe","login":"jdoe","pwd":"jdoepwd","email":"jdoe@cmp.com"}' http://localhost:8000/user-service/user
```
- Lister l'ensemble des utilisateurs à l'aide de votre WebBrowser
  
<img src="../step3/img/userlist-proxy-v1.jpg" alt="User List" width="400"/>

- Ajouter un hero via la commande curl suivante:

```
curl --header "Content-Type: application/json" --request POST --data '{ "name":"Thor","superPowerName":"Storm","superPowerValue":100,"imgUrl":"https://i.gifer.com/D2Tc.gif"}' http://localhost:8000/hero-service/hero
```

- Lister l'ensemble des heros à l'aide de votre WebBrowser
 <img src="../step3/img/herolist-proxy-v1.jpg" alt="Hero List" width="400"/>




## Ref
  - https://gitlab.com/help/user/packages/container_registry/index
  - https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html



      