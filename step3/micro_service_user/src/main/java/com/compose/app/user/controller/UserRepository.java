package com.compose.app.user.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.compose.app.user.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	public List<User> findByLogin(String login);
}
