package com.compose.app.user.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

public class UserDto {
	private Integer id;
	private String surName;
	private String lastName;
	private String login;
	private String email;
	private String pwd;

	public UserDto() {
	}

	public UserDto(Integer id, String surName, String lastName, String login, String email, String pwd) {
		super();
		this.id = id;
		this.surName = surName;
		this.lastName = lastName;
		this.login = login;
		this.email = email;
		this.pwd = pwd;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Override
	public String toString() {
		return "USER [" + this.id + "]: surName:" + this.surName + ", lastName:" + this.lastName + ", login:"
				+ this.login + " email:" + this.email;
	}
}
