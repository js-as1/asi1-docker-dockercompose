# Docker Compose, microservices et proxy

## 1. Contexte
Dans cette section nous allons mettre en plusieurs containers permettant de faire intéragir des micro services entre eux et de mettre en place un reverse proxy permettant de rediriger les requêtes provenant du Web Browser vers les micros services concernés. L'architecture cible est la suivante:

<img src="./img/DockerComposeStep3-Archi.jpg" alt="Targeted Architecture" width="600"/>

## 2. Mise en oeuvre sans proxy
### 2.1 Mise en place du contexte
- Récupérer les répertoires ```step3/base/micro_service_hero``` et ```step3/base/micro_service_user```. Chacun de ces deux répertoires contient un projet maven Springboot représentant un microserice et un ```Dockerfile``` associé (cf [step1](../step1/README.md))
  
### 2.2 Création du docker compose
- Créer le fichier ```docker-compose.yml``` comme suit:
``` yaml
version: "3"
services:
  heroapp:
    build:
      context: micro_service_hero # utilise un dockerfile and un répertoire particulier
      dockerfile: Dockerfile
    ports:
      - "8081:8081"
    depends_on: 
      - db_hero 
    environment: # Pass environment variables to the service
      SPRING_DATASOURCE_URL: jdbc:mysql://db_hero:3306/herobd?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false
      SPRING_DATASOURCE_USERNAME: heroname
      SPRING_DATASOURCE_PASSWORD: heropwd
    networks:
      - hero-network-private
      - hero-network-public
  db_hero:
    image: mysql:5.7
    ports:
      - "3306:3306"
    restart: always
    environment:
      MYSQL_DATABASE: herobd
      MYSQL_USER: heroname
      MYSQL_PASSWORD: heropwd
      MYSQL_ROOT_PASSWORD: heroRootpwd
    networks:
      - hero-network-private
  userapp:
    build:
      context: micro_service_user # utilise un dockerfile and un répertoire particulier
      dockerfile: Dockerfile
    ports:
      - "8082:8082"
    depends_on: 
      - db_user 
    environment: # Pass environment variables to the service
      SPRING_DATASOURCE_URL: jdbc:mysql://db_user:3306/userbd?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false
      SPRING_DATASOURCE_USERNAME: username
      SPRING_DATASOURCE_PASSWORD: userpwd
    networks:
      - user-network-private
      - user-network-public
  db_user:
    image: mysql:5.7
    ports:
      - "3306:3306"
    restart: always
    environment:
      MYSQL_DATABASE: userbd
      MYSQL_USER: username
      MYSQL_PASSWORD: userpwd
      MYSQL_ROOT_PASSWORD: userRootpwd

    networks:
      - user-network-private

networks:
  hero-network-private:
    internal: true
  user-network-private:
    internal: true
  hero-network-public:
  user-network-public:
```
- Explications

    ```yaml
     ...
     build:
      context: micro_service_hero # utilise un dockerfile and un répertoire particulier
      dockerfile: Dockerfile
      ...
    ```
    - Ici nous n'allons pas utiliser une image existante. Nous allons compiler une image docker à l'aide d'un ```Dockerfile```. Pour ce faire il faut spécifier le dossier ou se trouve le ```Dockerfile``` à l'aide du tag ```context```, ici ```context: micro_service_hero```. Ensuite on spécifie le fichier cible avec le tag ```dockerfile```, ici  ```dockerfile: Dockerfile```
    - Dans ce fichier de configuration les deux services ```db_hero``` et ```db_user``` bind la même adresse ```3306```. Ceci est possible car il utilise deux réseaux distincts, respectivement ```hero-network-private ``` et ```user-network-private ```.

### 2.3 Lancement des containers

- Compiler les deux projets maven et ```micro_service_hero``` ```micro_service_user```
```
mvn clean install
```
- les ```jars``` de chaque projet sont créés et disponibles pour la création des containers docker associés aux Dockerfiles
- Démarrer l'ensemble des containers en utilisant docker-compose
```
docker-compose up
```
- Vérifier que les images associées aux Dockerfiles ont bien été crées

```
docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
step3_userapp       latest              c1e9f350ba2a        7 hours ago         548MB
step3_heroapp       latest              f0f584202a70        7 hours ago         548MB
openjdk             8                   51d6b33ebe8a        11 days ago         511MB
mysql               5.7                 d05c76dbbfcf        2 weeks ago         448MB
```

- Vérifier que les containers docker associés au docker-compose ont bien été démarrés

```
docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
d0cd4b9c9d4e        step3_userapp       "java -jar SP-Compos…"   6 hours ago         Up 6 hours          0.0.0.0:8082->8082/tcp   step3_userapp_1
787fe20a827a        mysql:5.7           "docker-entrypoint.s…"   6 hours ago         Up 6 hours                                   step3_db_user_1
2e601118abe2        step3_heroapp       "java -jar SP-Compos…"   6 hours ago         Up 6 hours          0.0.0.0:8081->8081/tcp   step3_heroapp_1
9f13040d5ea9        mysql:5.7           "docker-entrypoint.s…"   6 hours ago         Up 6 hours                                   step3_db_hero_1
```

- Ajouter un utilisateur via la commande curl suivante:
```
curl --header "Content-Type: application/json" --request POST --data '{ "surName":"John","lastName":"Doe","login":"jdoe","pwd":"jdoepwd","email":"jdoe@cmp.com"}' http://localhost:8082/user
```
- Lister l'ensemble des utilisateurs à l'aide de votre WebBrowser
- 
<img src="./img/userlistv1.jpg" alt="User List" width="400"/>

- Ajouter un hero via la commande curl suivante:
```
curl --header "Content-Type: application/json" --request POST --data '{ "name":"Thor","superPowerName":"Storm","superPowerValue":100,"imgUrl":"https://i.gifer.com/D2Tc.gif"}' http://localhost:8081/hero
```
- Lister l'ensemble des heros à l'aide de votre WebBrowser
- 
<img src="./img/herolistv1.jpg" alt="Hero List" width="400"/>

## 3. Ajout d'un proxy
### 3.1 Création d'un proxy
- Créer un nouveau répertoire  ```proxy_server```
- Dans le répertoire créé, créer le fichier ```pom.xml``` suivant:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.1.7.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>com.compose</groupId>
	<artifactId>SP-Compose-Proxy-Step3</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>SP-Compose-Proxy-Step3</name>
	<description>Proxer application allowing to serve set of microservices</description>

	<properties>
		<java.version>1.8</java.version>
		<spring-cloud.version>Greenwich.RELEASE</spring-cloud.version>
		
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<!-- This dependency is added for zuul proxy server -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-zuul</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>
	
<!-- 	Avoid error like Caused by: java.lang.NoClassDefFoundError: org/springframework/boot/context/properties/ConfigurationPropertiesBean -->
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
```
- Créer les répertoires suivants:
  - ```src.main.java```
  - ```src.main.resources```

- Dans le répertoire ```src.main.resources```, créer le fichier ```application.properties``` comme suit:

```yaml
server.port = 8000
spring.application.name = zuulserver

zuul.routes.hero.path = /hero-service/**
zuul.routes.hero.url = http://heroapp:8081/
zuul.routes.hero.sensitiveHeaders = Cookie, Set-Cookie, Authorization

zuul.routes.user.path = /use-service/**
zuul.routes.user.url = http://userapp:8082/
zuul.routes.user.sensitiveHeaders = Cookie, Set-Cookie, Authorization

```
- Explications:
  - ``` zuul.routes.hero.path = /hero-service/** ``` crée une route ```hero``` si elle n'existe pas et associe toutes les requêtes commençant par ```/hero``` à la route en question
  - ```zuul.routes.hero.url = http://heroapp:8081/``` : définit l'URL de redirection pour cette route ici les paquets correspondants au pattern ```zuul.routes.hero.path``` seront redirigés vers ```http://heroapp:8081/```.
  - ``` zuul.routes.hero.sensitiveHeaders = Cookie, Set-Cookie, Authorization ``` définie une black list des paramètres de l'entête HTTP à ne pas transmettre aux services

- Dans le répertoire ```src.main.resources```, créer le package suivant ```com.compose.app```
- Dans ce package créer le fichier ```ProxyApp``` comme suit:

```java
package com.compose.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy // Using this annotation, we enable zuul proxy dependency.
@SpringBootApplication
public class ProxyApp {

	public static void main(String[] args) {
		SpringApplication.run(ProxyApp.class, args);
	}
}
```
- Créer le fichier ```Dockerfile``` associé à l'application ```proxy_server``` comme suit:

```docker
FROM openjdk:8
RUN mkdir -p /app/SP
WORKDIR /app/SP
COPY ./target/SP-Compose-Proxy-Step3-0.0.1-SNAPSHOT.jar SP-Compose-Proxy-Step3-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","SP-Compose-Proxy-Step3-0.0.1-SNAPSHOT.jar"]
```

### 3.2 Modification du docker-compose
- Une fois l'application proxy crée nous allons pouvoir modifier le docker compose pour qu'il puisse lié le proxy à nos micros services. Modifier le fichier ```docker-compose```s comme suit:

```yaml
version: "3"
services:
  proxyapp:
    build:
        context: proxy_server # utilise un dockerfile and un répertoire particulier
        dockerfile: Dockerfile
    ports:
      - "8000:8000"
    depends_on: 
      - heroapp
      - userapp
    networks:
      - hero-network-internal
      - user-network-internal
      - proxy-network-public

  heroapp:
    build:
      context: micro_service_hero # utilise un dockerfile and un répertoire particulier
      dockerfile: Dockerfile
    ports:
      - "8081:8081"
    depends_on: 
      - db_hero 
    environment: # Pass environment variables to the service
      SPRING_DATASOURCE_URL: jdbc:mysql://db_hero:3306/herobd?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false
      SPRING_DATASOURCE_USERNAME: heroname
      SPRING_DATASOURCE_PASSWORD: heropwd
    networks:
      - hero-network-private
      - hero-network-internal
  db_hero:
    image: mysql:5.7
    ports:
      - "3306:3306"
    restart: always
    environment:
      MYSQL_DATABASE: herobd
      MYSQL_USER: heroname
      MYSQL_PASSWORD: heropwd
      MYSQL_ROOT_PASSWORD: heroRootpwd
    networks:
      - hero-network-private
  userapp:
    build:
      context: micro_service_user # utilise un dockerfile and un répertoire particulier
      dockerfile: Dockerfile
    ports:
      - "8082:8082"
    depends_on: 
      - db_user 
    environment: # Pass environment variables to the service
      SPRING_DATASOURCE_URL: jdbc:mysql://db_user:3306/userbd?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false
      SPRING_DATASOURCE_USERNAME: username
      SPRING_DATASOURCE_PASSWORD: userpwd
    networks:
      - user-network-private
      - user-network-internal
  db_user:
    image: mysql:5.7
    ports:
      - "3306:3306"
    restart: always
    environment:
      MYSQL_DATABASE: userbd
      MYSQL_USER: username
      MYSQL_PASSWORD: userpwd
      MYSQL_ROOT_PASSWORD: userRootpwd
    networks:
      - user-network-private

networks:
  hero-network-private:
    internal: true
  user-network-private:
    internal: true
  hero-network-internal:
    internal: true
  user-network-internal:
    internal: true
  proxy-network-public:
```
- Explications:
  ```yaml
  proxyapp:
    build:
        context: proxy_server # utilise un dockerfile and un répertoire particulier
        dockerfile: Dockerfile
    ports:
      - "8080:8080"
    depends_on: 
      - heroapp
      - userapp
    networks:
      - hero-network-internal
      - user-network-internal
      - proxy-network-public
  ```
  - Déclaration du nouveau service ```proxyapp```. Tout comme les autres services ce dernier va construire une image docker basée sur le jar de l'application ```SP-Compose-Proxy-Step3```.
  - Ce service va utiliser les services ```heroapp``` et ```userapp```, c'est pourquoi il apparait dans la section ```depends_on```
  - Enfin les réseaux disponibles pour ce service lui permettront respectivement de communiquer avec ```heroapp``` (réseau ```hero-network-internal```), ```userapp``` (réseau ```user-network-internal```) et les clients Web (réseau ```proxy-network-public```)

  ```yaml
  ...
    networks:
        - user-network-private
        - user-network-internal
  ...
  ```
   -  Pour le service ```heroapp``` et ```userapp``` les réseaux ```user-network-public``` et ```hero-network-public``` sont devenus ```user-network-internal``` et ```hero-network-internal```

  ```yaml
  ...
  networks:
    hero-network-private:
      internal: true
    user-network-private:
      internal: true
    hero-network-internal:
      internal: true
    user-network-internal:
      internal: true
    proxy-network-public:
  ```
   - Dans cette configuration seul le réseau  `proxy-network-public ` sera accessible depuis l'extérieur. Les autres réseaux seront privés a usage interne de nos services.

La configuration finale est la suivante:

<img src="./img/DockerComposeStep3-finalNetworks.jpg" alt="Targeted Architecture" width="600"/>

### 3.3 Lancement des containers
- Compiler le nouveau projet maven et ```proxy_server``` 
```
mvn clean install
```
- le ```jars``` du projet est créé et disponible pour la création du container docker associé au Dockerfile
- Démarrer l'ensemble des containers en utilisant docker-compose
```
docker-compose up
```
- Vérifier que les images associées aux Dockerfiles ont bien été crées

```
docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
step3_proxyapp      latest              f8f3ab361395        2 minutes ago       546MB
step3_userapp       latest              c1e9f350ba2a        2 days ago          548MB
step3_heroapp       latest              f0f584202a70        2 days ago          548MB
openjdk             8                   51d6b33ebe8a        2 weeks ago         511MB
mysql               5.7                 d05c76dbbfcf        2 weeks ago         448MB
```

- Vérifier que les containers docker associés au docker-compose ont bien été démarrés

```
docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                    NAMES
76189b9a9920        step3_proxyapp      "java -jar SP-Compos…"   About a minute ago   Up About a minute   0.0.0.0:8000->8000/tcp   step3_proxyapp_1
d35137abb539        step3_heroapp       "java -jar SP-Compos…"   About a minute ago   Up About a minute                            step3_heroapp_1
271c70b281f5        step3_userapp       "java -jar SP-Compos…"   About a minute ago   Up About a minute                            step3_userapp_1
787fe20a827a        mysql:5.7           "docker-entrypoint.s…"   2 days ago           Up About a minute                            step3_db_user_1
9f13040d5ea9        mysql:5.7           "docker-entrypoint.s…"   2 days ago           Up About a minute                            step3_db_hero_1
```

- Ajouter un utilisateur via la commande curl suivante:
```
curl --header "Content-Type: application/json" --request POST --data '{ "surName":"John","lastName":"Doe","login":"jdoe","pwd":"jdoepwd","email":"jdoe@cmp.com"}' http://localhost:8000/user-service/user
```
- Lister l'ensemble des utilisateurs à l'aide de votre WebBrowser
  
<img src="./img/userlist-proxy-v1.jpg" alt="User List" width="400"/>

- Ajouter un hero via la commande curl suivante:

```
curl --header "Content-Type: application/json" --request POST --data '{ "name":"Thor","superPowerName":"Storm","superPowerValue":100,"imgUrl":"https://i.gifer.com/D2Tc.gif"}' http://localhost:8000/hero-service/hero
```

- Lister l'ensemble des heros à l'aide de votre WebBrowser
 <img src="./img/herolist-proxy-v1.jpg" alt="Hero List" width="400"/>