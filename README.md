# Déploiement de micro services à l'aide de Docker

## Step 1: Containerisation d'une application SpringBoot
[Step1](./step1/README.md)

## Step 2: Automatisation d'un déploiement d'application à l'aide de Docker Compose
[Step2](./step2/README.md)

## Step 3: Déploiement de micro services à l'aide de Docker Compose
[Step3](./step3/README.md)

## Step 4: Création automatique d'images docker à l'aide de CI/CD de Gitlab
[Step4](./step4/README.md)

## Quelques liens intéressants
- https://www.javainuse.com/devOps/docker/docker-networking
- https://www.javainuse.com/devOps/docker/docker-compose-tutorial
- https://springbootdev.com/2018/01/09/spring-boot-rest-api-with-docker-with-docker-compose/
- https://thepracticaldeveloper.com/2017/12/11/dockerize-spring-boot/
- https://www.callicoder.com/spring-boot-mysql-react-docker-compose-example/
- https://github.com/ankitrajput0096/Zuul_Proxy_SpringBoot